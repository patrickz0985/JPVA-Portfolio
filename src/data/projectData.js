import JPVAPortfolio from '../assets/portfolio-screenshot.png'
import ProjectImg from '../assets/ProjectImg.jpg'

const projectData = [
    {
      title: "JPVA Portfolio",
      imgUrl: `${JPVAPortfolio}`,
      stack: ["React", "Tailwind CSS", "JavaScript"],
      link: "https://github.com/joshpatrickz/jpva.github.io/",
    },
    {
      title: "Project 2",
      imgUrl: `${ProjectImg}`,
      stack: ["HTML", "CSS", "JavaScript"],
      link: "#",
    },
    {
      title: "Project 3",
      imgUrl: `${ProjectImg}`,
      stack: ["HTML", "CSS", "JavaScript"],
      link: "#",
    },
    {
      title: "Project 4",
      imgUrl: `${ProjectImg}`,
      stack: ["HTML", "CSS", "JavaScript"],
      link: "#",
    },
    {
      title: "Project 5",
      imgUrl: `${ProjectImg}`,
      stack: ["HTML", "CSS", "JavaScript"],
      link: "#",
    },
    {
      title: "Project 6",
      imgUrl: `${ProjectImg}`,
      stack: ["HTML", "CSS", "JavaScript"],
      link: "#",
    },
  ];
  
  export default projectData;